#t02_05_08_tictactoe

import os
import platform
import random

try:
	from colorama import init, Fore, Back, Style
	init()
	color = 1
except ImportError:
	print("Se recomienda la instalación de la librería <colorama>")
	input("Pulse <ENTER> para continuar...")
	color = 0
	

#None => None
def clear():
	if platform.system() == "Windows":
		os.system("cls")
	elif platform.system() == "Linux":
		os.system("clear")
		
#None => String (input)
def input_(msg="> "):
	try:
		val_input = input(msg)
	except KeyboardInterrupt:
		print("\n\nAdiós!")
		exit()
	except:
		print("\nAdiós!")
		exit()
	return val_input
	
#dict, list => dict
def mult_keys_sorted(d,keys,rev=0):
	for k in keys:
		d = dict(sorted(d.items(),key=lambda i:i[1][k],reverse=rev))
	return d
	
#GAME CLASS
class tictactoe:
	board = [0 for n in range(9)]
	board_rep = [" {} ".format(n) for n in range(9)]
	pos_free = [n for n in range(9)]
	width = 52
	mensaje = ""
	turno = 1
	game_status = 0
	game_mode = 0
	game_diff = 0
	game_initiated = 0
	player = 0
	pos_new = None
	pos_old = None

	#self => None (print)
	def print_tittle(self,space=0):
		tittle_string = ("{0} {1} "*4+"  T I C T A C T O E  "+
				"{0} {1} "*4).format("x","o")
		print(("\n{:^"+str(self.width)+"}").format(tittle_string))
		print("░"*self.width)
		if space:
			print()
	
	#self => None (print)
	def print_board(self):
		space_before_board = int(self.width/2 - 11/2) #board width
		print(( "{9:<"+str(space_before_board)+"}{0:^3}│{1:^3}│{2:^3}\n"
				"{9:<"+str(space_before_board)+"}═══╪═══╪═══\n"
				"{9:<"+str(space_before_board)+"}{3:^3}│{4:^3}│{5:^3}\n"
				"{9:<"+str(space_before_board)+"}═══╪═══╪═══\n"
				"{9:<"+str(space_before_board)+"}{6:^3}│{7:^3}│{8:^3}\n"
				).format(*self.board_rep,""))
				
	#self => None (print)
	def print_info(self):
		msg_mode = ("No seleccionado", "(1) Simple",
					"(2) Con reposición de piezas")
		msg_dif = ("No seleccionada", "(1) Fácil","(2) Normal",
					"(3) Difícil")
		simb0 = "-"
		if color:
			simb1=Fore.GREEN+"▲"+Style.RESET_ALL
			simb2=Fore.CYAN+"▼"+Style.RESET_ALL
		else:
			simb1="▲"
			simb2="▼"
		players_name = ("DESCONOCIDO","JUGADOR","MÁQUINA")
		ancho_players = [len(p)+5 for p in players_name]
		msg_player = ("[{}]  {}".format(simb0,players_name[0]),
					  "[{}]  {}".format(simb1,players_name[1]),
					  "[{}]  {}".format(simb2,players_name[2]))
		print("{:<17}{}".format("Modo de Juego:",msg_mode[self.game_mode]))
		print("{:<17}{}".format("Dificultad:", msg_dif[self.game_diff]))
		print("─"*self.width)
		string_turn = "{0}: [{1}]".format("Turno",self.turno)
		string_player = msg_player[self.player]
		print(("{0:<"+str(self.width-ancho_players[self.player])+"}{1}"
				).format(string_turn,string_player))
		print()
				
	#self => None (print)
	def print_msg(self):
		if self.game_status:
			num_mar = self.width-len(self.mensaje)
			if num_mar%2==0:
				print(("{0} {1} {0}").format(":"*((num_mar//2)-1),self.mensaje))
			else:
				print(("{0} {1} {0}:").format(":"*((num_mar//2)-1),self.mensaje))
		else:
			print(("::{:^"+str(self.width-4)+"}::").format(self.mensaje))
				
	#self => None (print)
	def print_game(self):
		clear()
		if color:
			f1 = Back.GREEN+Fore.BLACK+" X "+Style.RESET_ALL
			f2 = Back.CYAN+Fore.BLACK+" O "+Style.RESET_ALL
			if self.game_status == 0:
				if (self.player==1 and self.game_mode==2 and 
				len(self.pos_free)==3):
					f1 = Back.GREEN+Fore.BLACK+" {} "+Style.RESET_ALL
				elif (self.player==2 and self.game_mode==2 and 
				len(self.pos_free)==3):
					f2 = Back.CYAN+Fore.BLACK+" {} "+Style.RESET_ALL
		else:
			f1 = "███"
			f2 = "░░░"
			if self.game_status == 0:
				if (self.player==1 and self.game_mode==2 and 
				len(self.pos_free)==3):
					f1 = "█{}█"
				elif (self.player==2 and self.game_mode==2 and 
				len(self.pos_free)==3):
					f2 = "░{}░"
		self.board_rep = [[" {} ",f1,f2][self.board[n]].format(n)
						 for n in range(9)]
		self.print_tittle(space=1)
		self.print_info()
		self.print_board()
		self.print_msg()
		
	#self, int => list (int)
	def pos_redor(self,pos):
		sum_redor = ((-3,-2,1,3,4),(-4,-3,-2,-1,1,2,3,4),(-4,-3,-1,2,3))
		col = pos%3
		pos_redor = [pos+s for s in sum_redor[col]]
		return pos_redor
	
	#self, int => list (int)
	def pos_redor_libres(self,pos):
		all_pos_redor = self.pos_redor(pos)
		free_pos_redor = [p for p in all_pos_redor if p in self.pos_free]
		return free_pos_redor
		
	#self => int / None
	def input_pos(self):
		print("\n¿Donde quieres colocar una ficha?\n")
		pos = input_()
		if pos.isdigit() and int(pos) in self.pos_free:
			return int(pos)
		else:
			if pos.isdigit() and int(pos) in range(9):
				self.mensaje = "Esa casilla ya está ocupada."
			else:
				self.mensaje = "Debes introducir una casilla válida."
			return None
			
	#self => int / none
	def move_pos_old(self):
		fichas = [x for x in range(9) if self.board[x]==1]
		dict_fichas = {x:self.pos_redor_libres(x) for x in fichas}
		fichas_validas = [x for x,y in dict_fichas.items() if y]
		print("\n¿Qué ficha quieres mover {}?\n".format(fichas_validas))
		pos_old = input_()
		if pos_old.isdigit() and int(pos_old) in fichas_validas:
			pos_old = int(pos_old)
			return pos_old
		else:
			self.mensaje = "Debes introducir una casilla válida."
			return None
	
	#self => int / none		
	def move_pos_new(self):
		pos_redor = self.pos_redor_libres(self.pos_old)
		print("\n¿A donde la quieres mover {}?\n".format(pos_redor))
		pos_new = input_("> [{}] => ".format(self.pos_old))
		if pos_new.isdigit() and int(pos_new) in pos_redor:
			pos_new = int(pos_new)
			return pos_new
		else:
			self.mensaje = "No puedes mover la ficha a esa casilla"
			return None
			
	#self => Boolean / (exit)
	def input_remake(self):
		print("\n¿Deseas volver a jugar (s/n)?\n")
		respuesta = input_()
		if respuesta.upper() == "S":
			return 1
		elif respuesta.upper() == "N":
			exit()
		else:
			return 0
	
	#self => None
	def update_pos_free(self):
		self.pos_free = [n for n in range(9) if not self.board[n]]
	
	#self => None
	def update_game_status(self):
		rows = [self.board[n:n+3] for n in range(0,9,3)]
		cols = [self.board[n:9:3] for n in range(3)]
		diagonal = [self.board[0:9:4],self.board[2:7:2]]
		for linea in rows+cols+diagonal:
			if linea == [1,1,1]:
				self.game_status = 1
				self.mensaje = "▲ VICTORIA DEL JUGADOR ▲"
				return
			elif linea == [2,2,2]:
				self.game_status = 2
				self.mensaje = "▼ VICTORIA DE LA MÁQUINA ▼"
				return
		if self.pos_free:
			self.game_status = 0
		else:
			self.game_status = 3
			self.mensaje = "■ EMPATE ■"
			
	#self, list => dict
	def dict_lines(self,board):
		rows = {(n,n+1,n+2):board[n:n+3] for n in range(0,9,3)}
		cols = {(n,n+3,n+6):board[n:9:3] for n in range(3)}
		diagonal = {(0,4,8):board[0:9:4],(2,4,6):board[2:7:2]}
		lines = {**rows,**cols,**diagonal}
		return lines
		
	#self, list => list		
	def com_pos_types(self,board):
		lines = self.dict_lines(board)
		pos_types = {"win":[],"lost":[],"aggro":[],"prev":[]}
		for casillas,fichas in lines.items():
			#Pos ganadoras
			if fichas.count(2) == 2 and 0 in fichas:
				pos_types["win"].append(casillas[fichas.index(0)])
			#Pos ganadoras para el jugador
			if fichas.count(1) == 2 and 0 in fichas:
				pos_types["lost"].append(casillas[fichas.index(0)])
			#Pos que posibilitan victoria en 2 turnos
			if fichas.count(0) == 2 and 2 in fichas:
				pos_types["aggro"] += [casillas[i] for i,f in enumerate(fichas)
										if not f]
			#Pos que posibilitan la victoria del jugador en 2 turnos
			if fichas.count(0) == 2 and 1 in fichas:
				pos_types["prev"] += [casillas[i] for i,f in enumerate(fichas)
										if not f]
		#Pos que posibilitan la victoria del jugador en 2 turnos por 2 vías	
		pos_types["prev+"] = [p for p in pos_types["prev"] if 
								pos_types["prev"].count(p) > 1]
		return pos_types
		
	#self, list => list
	def com_pos_types_m2(self,board):
		lines = self.dict_lines(board)
		fichas_2 = [x for x in range(9) if self.board[x]==2]
		fichas_1 = [x for x in range(9) if self.board[x]==1]
		dict_movements_comp = {p:self.pos_redor_libres(p) for p in fichas_2}
		dict_movements_player = {p:self.pos_redor_libres(p) for p in fichas_1}
		pos_types = {"win":[],"lost":[],"danger":[]}
		for casillas,fichas in lines.items():
			#Movs ganadores
			if fichas.count(2) == 2 and 0 in fichas:
				pos_fichas = [x for x in range(3) if fichas[x]]
				casillas_ocupadas = [casillas[p] for p in pos_fichas]
				casilla_libre = casillas[fichas.index(0)]
				ficha_libre = list(set(fichas_2)-set(casillas_ocupadas))[0]
				if casilla_libre in dict_movements_comp[ficha_libre]:
					pos_types["win"].append((ficha_libre,casilla_libre))
			#Movs para evitar que el jugador gane
			if fichas.count(1) == 2 and 0 in fichas:
				pos_fichas_player = [x for x in range(3) if fichas[x]]
				casillas_ocupadas = [casillas[p] for p in pos_fichas_player]
				casilla_libre = casillas[fichas.index(0)]
				ficha_libre_p1 = list(set(fichas_1)-set(casillas_ocupadas))[0]
				if casilla_libre in dict_movements_player[ficha_libre_p1]:
					for p,movs in dict_movements_comp.items():
						if casilla_libre in movs:
							pos_types["lost"].append((p,casilla_libre))
			#Fichas que moverlas permiten que el jugador gane
			if fichas.count(1) == 2 and 2 in fichas:
				casilla_computer = casillas[fichas.index(2)]
				dict_redor_p1 = {p:self.pos_redor(p) for p in fichas_1}
				pos_redor_player = sum([x for x in dict_redor_p1.values()],[])
				if casilla_computer in pos_redor_player:
					pos_types["danger"].append(casilla_computer)
		return pos_types
			
			
	#self => int (0-8)
	def computer_move_easy(self):
		n_fichas_2 = self.board.count(2)
		if self.game_mode==1 or (self.game_mode==2 and n_fichas_2 < 3):
			chosen_pos = random.choice(self.pos_free)
			return chosen_pos
		else:
			fichas_2 = [x for x in range(9) if self.board[x]==2]
			dict_fichas = {x:self.pos_redor_libres(x) for x in fichas_2}
			fichas_validas = [x for x,y in dict_fichas.items() if y]
			chosen_pos_old = random.choice(fichas_validas)
			pos_redor = dict_fichas[chosen_pos_old]
			chosen_pos_new = random.choice(pos_redor)
			return chosen_pos_old,chosen_pos_new
		
	#self => int (0-8)
	def computer_move_normal(self):
		n_fichas_2 = self.board.count(2)
		if self.game_mode==1 or (self.game_mode==2 and n_fichas_2 < 3):
			pos_types = self.com_pos_types(self.board)
			if pos_types["win"]:
				chosen_pos = random.choice(pos_types["win"])
			elif pos_types["lost"]:
				chosen_pos = random.choice(pos_types["lost"])
			else:
				chosen_pos = random.choice(self.pos_free)
			return chosen_pos
		else:
			pos_types = self.com_pos_types_m2(self.board)
			if pos_types["win"]:
				tuple_chosen_pos = random.choice(pos_types["win"])
				chosen_pos_old = tuple_chosen_pos[0]
				chosen_pos_new = tuple_chosen_pos[1]
			elif pos_types["lost"]:
				if pos_types["danger"]:
					pos_lost_no_danger = [t for t in pos_types["lost"] if
						t[0] not in pos_types["danger"]]
					if pos_lost_no_danger:
						tuple_chosen_pos = random.choice(pos_lost_no_danger)
					else:
						tuple_chosen_pos = random.choice(pos_types["lost"])
				else:
					tuple_chosen_pos = random.choice(pos_types["lost"])
				chosen_pos_old = tuple_chosen_pos[0]
				chosen_pos_new = tuple_chosen_pos[1]
			else:
				fichas_2 = [x for x in range(9) if self.board[x]==2]
				dict_fichas = {x:self.pos_redor_libres(x) for x in fichas_2}
				fichas_validas = [x for x,y in dict_fichas.items() if y]
				fichas_validas_no_danger = list(set(fichas_validas)-
						set(pos_types["danger"]))
				if fichas_validas_no_danger:
					chosen_pos_old = random.choice(fichas_validas_no_danger)
				else:
					chosen_pos_old = random.choice(fichas_validas)
				pos_redor = dict_fichas[chosen_pos_old]
				chosen_pos_new = random.choice(pos_redor)
			return chosen_pos_old,chosen_pos_new
			
	#self => int(0-8)
	def computer_move_hard(self):
		pos_types = self.com_pos_types(self.board)
		if pos_types["win"]:
			chosen_pos = random.choice(pos_types["win"])
		elif pos_types["lost"]:
			chosen_pos = random.choice(pos_types["lost"])
		elif pos_types["aggro"]:
			dict_pos = {p:{} for p in self.pos_free}
			for p in self.pos_free:
				fut_board = self.board[:]
				fut_board[p] = 2
				fut_pos_types = self.com_pos_types(fut_board)
				dict_pos[p]["n_wins"] = len(fut_pos_types["win"])
				dict_pos[p]["n_aggro"] = len(fut_pos_types["aggro"])
				dict_pos[p]["nocounter"] = 1
				dict_pos[p]["nocounter2"] = 1
				for pw in fut_pos_types["win"]:
					if pw in fut_pos_types["prev"] and dict_pos[p]["n_wins"] < 2:
						dict_pos[p]["nocounter"] = 0
					if pw in fut_pos_types["prev+"] and dict_pos[p]["n_wins"] < 2:
						dict_pos[p]["nocounter2"] = 0
			#Ordenamos el diccionario según la prioridad
			sorted_keys = ["n_aggro","n_wins","nocounter2","nocounter"]
			dict_pos = mult_keys_sorted(dict_pos,sorted_keys,rev=1)
			for p,i in dict_pos.items():
				d = sorted(i)
			max_values = list(dict_pos.values())[0]
			pos_best = [p for p,i in dict_pos.items()
						if i["n_aggro"] == max_values["n_aggro"] and
						i["n_wins"] == max_values["n_wins"] and
						i["nocounter"] == max_values["nocounter"] and
						i["nocounter2"] == max_values["nocounter2"]]
			chosen_pos = random.choice(pos_best)
		elif pos_types["prev"]:
			dict_pos = {p:{} for p in self.pos_free}
			for p in self.pos_free:
				fut_board = self.board[:]
				fut_board[p] = 2
				fut_pos_types = self.com_pos_types(fut_board)
				dict_pos[p]["n_prev"] = len(fut_pos_types["prev"])
				dict_pos[p]["n_prev+"] = len(fut_pos_types["prev+"])
				for p2 in fut_pos_types["prev"]:
					fut_board_2 = fut_board[:]
					fut_board_2[p2] = 1
					fut_pos_types_2 = self.com_pos_types(fut_board_2)
					dict_pos[p]["n_prev"] += len(fut_pos_types_2["prev"])
					dict_pos[p]["n_prev+"] += len(fut_pos_types_2["prev+"])
			#Ordenamos el diccionario según la prioridad
			sorted_keys = ["n_prev","n_prev+"]
			dict_pos = mult_keys_sorted(dict_pos,sorted_keys)
			for p,i in dict_pos.items():
				d = sorted(i)
			max_values = list(dict_pos.values())[0]
			pos_best = [p for p,i in dict_pos.items()
						if i["n_prev"] == max_values["n_prev"] and
						i["n_prev+"] == max_values["n_prev+"]]
			chosen_pos = random.choice(pos_best)
		else:
			chosen_pos = random.choice(self.pos_free)
		return chosen_pos
			
	#self => int (0-8)
	def computer_choice(self):
		if self.game_diff == 1:
			return self.computer_move_easy()
		elif self.game_diff == 2:
			return self.computer_move_normal()
		elif self.game_diff == 3:
			return self.computer_move_hard()
			
	#self => None
	def print_initiate_subtittle(self):
		print(("░░{:^"+str(self.width-4)+"}░░").format(""))
		print(("░░{:^"+str(self.width-4)+"}░░").format("BIENVENIDO/A"))
		print(("░░{:^"+str(self.width-4)+"}░░").format(""))
		print(("::{:^"+str(self.width-4)+"}::").format(""))
		msg_numcas = "Cada casilla tiene asignado un número (0-8)"
		print(("::{:^"+str(self.width-4)+"}::\n").format(msg_numcas))
		self.print_board()
		self.print_msg()
		print()
	
	#self => None
	def input_choose_mode(self):
		print("Elija el modo de juego:\n")
		print("{:<4}1). Simple".format(""))
		print("{:<4}2). Con reposición de piezas".format(""))
		print()
		self.game_mode = input_()
		if self.game_mode and self.game_mode in ("1","2"):
			self.game_mode = int(self.game_mode)
			self.mensaje = ""
		else:
			self.mensaje = "Elija un modo de juego válido"
			self.game_mode = False
			
	#self => None
	def input_choose_ia(self):
		print("Elija ahora la IA del oponente:\n")
		print("{:<4}1). Fácil".format(""))
		print("{:<4}2). Normal".format(""))
		if self.game_mode == 1:
			print("{:<4}3). Difícil".format(""))
			range_mode = ("1","2","3")
		else:
			range_mode = ("1","2")
		print()
		self.game_diff = input_()
		if self.game_diff and self.game_diff in range_mode:
			self.game_diff = int(self.game_diff)
			self.mensaje = ""
		else:
			self.mensaje = "Elija un nivel de dificultad válido"
			self.game_diff = False
			
	#self => None
	def print_who_start(self):
		self.player = random.randint(1,2)
		print("Perfecto. Ahora vamos a ver quién empieza:")
		if self.player == 1:
			msg_start = "EMPIEZA EL JUGADOR"
			print(("\n▲ {:^"+str(self.width-4)+"} ▲\n").format(msg_start))
		else:
			msg_start = "EMPIEZA LA MÁQUINA"
			print(("\n▼ {:^"+str(self.width-4)+"} ▼\n").format(msg_start))
		self.game_initiated = 1
		input_("Pulsa ENTER para continuar... ")
		
	#self => None		
	def game_initiate(self):
		while not(self.game_mode and self.game_diff and self.player):
			clear()
			self.print_tittle()
			self.print_initiate_subtittle()
			if self.game_mode:
				if self.game_diff:
					self.print_who_start()
				else:
					self.input_choose_ia()
			else:	
				self.input_choose_mode()
				
	#self => None
	def player_turn(self):
		n_fichas_1 = self.board.count(1)
		if self.game_mode==1 or (self.game_mode==2 and n_fichas_1 < 3):
			pos = self.input_pos()
			if pos in range(9):
				self.board[pos] = 1
				self.update_pos_free()
				self.update_game_status()
				if self.game_status == 0:
					self.player = 2
					self.turno += 1
					self.mensaje = ""
		else:
			if self.pos_old in range(9) and self.pos_new in range(9):
				self.board[self.pos_old] = 0
				self.board[self.pos_new] = 1
				self.update_pos_free()
				self.update_game_status()
				self.pos_old = None
				self.pos_new = None
				if self.game_status == 0:
					self.player = 2
					self.turno += 1
					self.mensaje = ""
			elif self.pos_old in range(9):
				self.pos_new = self.move_pos_new()
			else:
				self.pos_old = self.move_pos_old()
				
	#self => None
	def computer_turn(self):
		n_fichas_2 = self.board.count(2)
		if self.game_mode==1 or (self.game_mode==2 and n_fichas_2 < 3):
			pos_choice = self.computer_choice()
			print("\nMueve la MÁQUINA.\n")
			self.board[pos_choice] = 2
		else:
			pos_old,pos_new = self.computer_choice()
			print("\nMueve la MÁQUINA: [{}] => [{}]\n".format(
					pos_old,pos_new))
			self.board[pos_old] = 0
			self.board[pos_new] = 2
		self.update_pos_free()
		self.update_game_status()
		if self.game_status == 0:
			self.player = 1
			self.turno += 1
		input_("Pulsa ENTER para continuar... ")
						
	#GAME LOOP
	def main(self):
		while 1:
			if self.game_initiated:
				self.print_game()
				#ESTADO: JUGANDO
				if self.game_status == 0:
					if self.game_status == 0:
						#PLAYER TURN
						if self.player == 1:
							self.player_turn()
						#COMPUTER TURN
						elif self.player == 2:
							self.computer_turn()
				#ESTADO: FIN DE JUEGO
				elif self.game_status in (1,2,3):
					if self.input_remake():
						self.board = [0 for n in range(9)]
						self.board_rep = [" {} ".format(n) for n in range(9)]
						self.pos_free = [n for n in range(9)]
						self.mensaje = ""
						self.turno = 1
						self.game_status = 0
						self.player = random.randint(1,2)
			else:
				self.game_initiate()
				
	#START
	def start(self):
		self.main()
						
#Launch GAME						
if __name__ == "__main__":
	game = tictactoe()
	game.start()
