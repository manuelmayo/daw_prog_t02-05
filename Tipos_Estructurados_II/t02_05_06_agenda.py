#t02_05_06_agenda

import os.path
import json

#None => None (print)
def main():
	json_filename = "t02_05_06_agenda.json"
	agenda = load_dict_json(json_filename)
	opcions_val = ("N","E","A","M","B","G","S")
	while 1:
		opcion = False
		while not opcion:
			imprimir_opciones()
			opcion = input_gestion("> Opción: ","Agenda cerrada.")
			if opcion in opcions_val:
				if opcion == "N": #NUEVO CONTACTO
					nuevo_contacto(agenda)
				if opcion == "E": #ELIMINAR CONTACTO
					eliminar_contacto(agenda)
				if opcion == "A": #AÑADIR DATOS
					add_data(agenda)
				if opcion == "M": #MOSTRAR AGENDA
					mostrar_contactos(agenda)
				if opcion == "B": #BUSCAR CONTACTO
					buscar_contacto(agenda)
				if opcion == "G": #GRABAR AGENDA
					write_dict_json(agenda,json_filename)
				if opcion == "S": #SALIR
					print("\nAgenda Cerrada.")
					exit()
			elif opcion == 0:
				exit()
			else:
				print("\nLa opción introducida no es válida")
				
#None => None (print)
def imprimir_opciones():
	print("\n{0} A G E N D A {0}".format("#"*8))
	print("N:  Nuevo Contacto")
	print("E:  Eliminar Contacto")
	print("A:  Añadir Datos a un Contacto")
	print("M:  Mostrar Agenda")
	print("B:  Buscar Contacto")
	print("G:  Grabar Agenda")
	print("S:  Salir")

#string,string,boolean => string
def input_gestion(minput,merror):
	try:
		opcion = input("\n{}".format(minput))
	except KeyboardInterrupt:
		print("\n\n{}".format(merror))
		return False
	except:
		print("\n{}".format(merror))
		return False
	return opcion.upper()
	
#string,list => None (print)
def mostrar_contacto(nick,listdatos):
	print("\n"+"-"*29)
	print("Nick: {}".format(nick))
	print("Nombre Completo: {}".format(listdatos[0]))
	if listdatos[1]:
		print("Datos del Contacto (tlf/email/dir):")
		for d in listdatos[1]:
			print("\t"+d)

#dict => None (print)
def mostrar_contactos(contactos):
	if contactos:
		for nick,namedata in sorted(contactos.items()):
			mostrar_contacto(nick,namedata)
	else:
		print("La agenda está vacía")
		
#dict => None (print) | add elements in list
def add_data(contactos):
	nick = False
	merror = "Adición de Datos de un Contacto cancelada"
	while not nick:
		nick = input_gestion("Nick: ",merror)
		if nick:
			if nick in contactos:
				mostrar_contacto(nick,contactos[nick])
				print("\nIntroduzca datos del contacto (tlf,email/dir):")
				print("Introduzca uno por linea")
				print("Linea en blanco para finalizar")
				linea_datos = 1
				while linea_datos:
					try:
						linea_datos = input()
					except:
						print("\nNuevo Contacto cancelado")
						return
					if linea_datos:
						contactos[nick][1].append(linea_datos)
		elif nick == 0:
			return
			
#dict => None (print)
def buscar_contacto(contactos):
	nick = False
	merror = "Busqueda cancelada"
	while not nick:
		nick = input_gestion("Nick: ",merror)
		if nick:
			if nick in contactos:
				mostrar_contacto(nick,contactos[nick])
				return
			else:
				print("\nNo existe ningún contacto con ese Nick")
				return
		elif nick == 0:
			return 
		
#dict => None | delete element in dict
def eliminar_contacto(contactos):
	nick = False
	merror = "Eliminación de Contacto cancelada"
	while not nick:
		nick = input_gestion("Nick: ",merror)
		if nick:
			if nick in contactos:
				del contactos[nick]
				print("\nContacto eliminado")
				return
			else:
				print("\nNo existe ningún contacto con ese Nick")
				return
		elif nick == 0:
			return

#dict => #None | insert element in dict
def nuevo_contacto(contactos):
	nick = False
	merror = "Nuevo Contacto cancelado"
	while not nick:
		nick = input_gestion("Nick: ",merror)
		if nick in contactos:
			nick = False
			print("\nYa existe un contacto con ese nick")
		elif nick == 0:
			return
	nombre = False
	while not nombre:
		nombre = input_gestion("Nombre Completo: ",merror)
		if nombre == 0:
			return
	print("\nIntroduzca datos del contacto (tlf,email/dir):")
	print("Introduzca uno por linea")
	print("Linea en blanco para finalizar")
	datos = []
	linea_datos = 1
	while linea_datos:
		linea_datos = input_gestion("",merror)
		if linea_datos:
			datos.append(linea_datos)
		elif linea_datos == 0:
			return
	contactos[nick] = [nombre,datos]
	input("\nContacto añadido. Pulse ENTER para continuar...")

#None => dict
def load_dict_json(filename):
	if os.path.isfile(filename):
		archivo_json = open(filename, "r")
		contenido_json = archivo_json.read()
		if contenido_json:
			return json.loads(contenido_json)
		else:
			return {}
	else:
		return {}
	
#dict => None (write file)
def write_dict_json(contactos, filename):
	agenda_json = json.dumps(contactos)
	archivo_json = open(filename, "w")
	archivo_json.write(agenda_json)
	archivo_json.close()
	print("\nAgenda Grabada")
	

if __name__ == "__main__":
	main()
