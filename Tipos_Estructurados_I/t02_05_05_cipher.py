#t02_05_05_cipher

from t02_05_mod import elimina_tildes

#None => None (print)
def main():
	key = ""
	opcions_val = ("C","D","K","M","S")
	while 1:
		opcion = False
		while not opcion:
			imprimir_opciones()
			try:
				opcion = input("\n> Opción: ")
			except:
				print("\n\nBye!")
				exit()
			opcion = opcion.upper()
			if opcion in opcions_val:	
				if opcion == "C":
					if key:
						cifrar(key)
					else:
						opcion = False
						print("\nNo se ha introducido la clave de cifrado")
				if opcion == "D":
					if key:
						cifrar(key,False)
					else:
						opcion = False
						print("\nNo se ha introducido la clave de cifrado")
				if opcion == "K":
					key = establecer_clave()
				if opcion == "M":
					if key:
						mostrar_clave(key)
					else:
						print("\nNo hai ninguna clave establecida")
				if opcion == "S":
					print("\nBye!")
					exit()
			else:
				opcion = False
				print("\nLa opcion introducida no es válida")

#None => None (print)
def imprimir_opciones():
	print("\n{0} C I P H E R {0}".format("#"*8))
	print("C:     Cifrar")
	print("D:     Descifrar")
	print("K:     Establecer Clave")
	print("M:     Mostrar Clave")
	print("S:     Salir")

#None => string / False
def establecer_clave():
	key = False
	while not key:
		try:
			key = input("\nIntroduce la clave de cifrado: ")
		except:
			print("\n\nEstablecimiento de la clave cancelado")
			return False
		if key.isalpha() and not "ñ" in key:
			key = elimina_tildes(key)
			return key.upper()
		else:
			key = False
			print("\nLa clave solo puede tener caracteres alfabéticos sin repetir")
			
#String => None (print)
def mostrar_clave(key):
	abc = "ABCDEFGHIJKLMNOPQRSTUWXYZ"
	keyabc = key+"".join((l for l in abc if l not in key))
	print("\n"+"".join("{:^2}".format(l) for l in abc))
	print("".join("{:^2}".format(l) for l in keyabc))
	
#String => None (print)
def cifrar(key,cifrar=1):
	mensaje = ""
	while not mensaje:
		eof = False
		try:
			print("Introduce el mensaje (^D/^Z para terminar):")
			while not eof:	
				mensaje += input()+"\n"
		except EOFError:
			eof = True
		except KeyboardInterrupt:
			if cifrar:
				print("\n\nCifrado de mensaje cancelado")
			else:
				print("\n\nDescifrado de mensaje cancelado")
			exit()
		if mensaje:
			abc = "ABCDEFGHIJKLMNOPQRSTUWXYZ"
			keyabc = key+"".join((l for l in abc if l not in key))
			if cifrar:
				parejas = dict(zip(abc,keyabc))
			else:
				parejas = dict(zip(keyabc,abc))
			mensaje_cifrado = ""
			for l in mensaje.upper():
				if l in abc:
					mensaje_cifrado += parejas[l]
				else:
					mensaje_cifrado += l
			print("\n"+">"*5+"\n")
			print(mensaje_cifrado)
		else:
			mensaje = ""
			print("\nDebes introducir un mensaje")


if __name__ == "__main__":
	main()
