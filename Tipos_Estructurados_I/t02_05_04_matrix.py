#t02_05_04_matrix

#None => None (print)
def main():
	matriz = []
	opcions_val = ("I","M","C","S")
	while 1:
		opcion = False
		while not opcion:
			imprimir_opciones()
			try:
				opcion = input("\n> Opción: ")
			except:
				print("\nBye!")
				exit()
			opcion = opcion.upper()
			if opcion in opcions_val:
				if opcion == "I":
					matriz = iniciar_matriz(matriz)
				if opcion == "M":
					if not matriz:
						print("\nLa matriz no está inicializada!!")
					else:
						mostrar_matriz(matriz)
				if opcion == "C":
					if not matriz:
						print("\nLa matriz no está inicializada!!")
					else:
						matriz = cambiar_valor(matriz)
				if opcion == "S":
					print("Bye!")
					exit()
			else:
				print("\nLa opción introducida no es válida")
				
#None => None (print)
def imprimir_opciones():
	print("\n{0} M A T R I X {0}".format("#"*8))
	print("I:  Iniciar Matriz")
	print("M:  Mostrar Matriz")
	print("C:  Cambiar Valor")
	print("S:  Salir")
	
	
#None => list (of list)
def iniciar_matriz():
		nfilas = False
		while not nfilas:
			try:
				nfilas = input("Número de filas [1-99]: ")
			except:
				print("\nInicialización de la matriz cancelada")
				return None
			if nfilas.isdecimal() and int(nfilas) > 0 and int(nfilas) <= 99:
				nfilas = int(nfilas)
			else:
				nfilas = False
				print("El número de filas debe ser un número entero entre 1 y 99")
		ncolumnas = False
		while not ncolumnas:
			try:
				ncolumnas = input("Número de columnas [1-99]: ")
			except:
				print("\nInicialización de la matriz cancelada")
				return None
			if ncolumnas.isdecimal() and int(ncolumnas) > 0 and int(ncolumnas) <= 99:
				ncolumnas = int(ncolumnas)
			else:
				nfilas = False
				print("El número de columnas debe ser un número entero entre 1 y 99")
		valor = False
		while not valor:
			try:
				valor = input("Valor inicial para las celdas: ")
			except:
				print("\nInicialización de la matriz cancelada")
				return None
			try:
				if valor.isdigit():
					valor = int(valor)
				else:
					valor = float(valor)
				matriz = [[valor for x in range(ncolumnas)] for y in range(nfilas)]
				return matriz
			except:
				valor = False
				print("El valor debe ser un número")


#list (of list) => None (print)
def mostrar_matriz(m):
	nrow = len(m) #número de filas
	ncol = len(m[0]) #numero de columnas
	col_s_def = 5 #tamaño de cada columna por defecto cuando se muestra en consola
	cols_s_l = [max([max(col_s_def,len(str(r[c]))) for r in m]) for c in range(ncol)]
	total_long = sum([s+1 for s in cols_s_l]) #por las barras
	col_format_def = "{:^"+str(col_s_def)+"}"
	for i in range(ncol): #encabezado
		if i==0:
			print("\n"+col_format_def.format(""),end="")
		col_size = cols_s_l[i]
		col_format = "{:^"+str(col_size)+"}"
		if i==ncol-1:
			print("|"+col_format.format(i))
		else:
			print("|"+col_format.format(i),end="")
	for r in range(nrow): #filas
		print("-"*total_long+"-"*(col_s_def))
		print(col_format_def.format(r),end="")
		for c in range(ncol):
			col_size = cols_s_l[c]
			col_format = "{:^"+str(col_size)+"}"
			if c==ncol-1:
				print("|"+col_format.format(m[r][c]))
			else:
				print("|"+col_format.format(m[r][c]),end="")
				
#list (of list) => list (of list)
def cambiar_valor(m):
	nrow = len(m) #número de filas
	ncol = len(m[0]) #numero de columnas
	nrow_sel = False
	while not nrow_sel:
		try:
			nrow_sel = input("Número de fila [0-{}]: ".format(nrow-1))
		except:
			print("\nCambio de valor en la matriz cancelado")
			return None
		if not (nrow_sel.isdecimal() and int(nrow_sel) >= 0 and int(nrow_sel) < nrow):
			nrow_sel = False
			print("El número de fila debe ser un número entero entre 0 y {}".format(
					nrow-1))
	ncol_sel = False
	while not ncol_sel:
		try:
			ncol_sel = input("Número de columna [0-{}]: ".format(ncol-1))
		except:
			print("\nCambio de valor en la matriz cancelado")
			return None
		if not (ncol_sel.isdecimal() and int(ncol_sel) >= 0 and int(ncol_sel) < ncol):
			ncol_sel = False
			print("El número de columna debe ser un número entero entre 0 y {}".
					format(ncol-1))
	valor = False
	while not valor:
		try:
			valor = input("Introduzca el valor: ")
		except:
			print("\nCambio de valor en la matriz cancelado")
			return None
		try:
			if valor.isdigit():
				valor = int(valor)
			else:
				valor = float(valor)
			m[int(nrow_sel)][int(ncol_sel)] = valor
			return m
		except:
			valor = False
			print("El valor debe ser un número")


if __name__ == "__main__":
	main()
