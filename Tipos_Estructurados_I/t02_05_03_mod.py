#t02_05_mod

""" Module remove_dup(), elimina_tildes_tup() y elimina_tildes().
DAW 1 Modular - programmation"""

#secuence (list, tuple, string...) => tuple
def remove_dup(sec):
	"""Dada cualquier secuencia iterable (tupla, lista, string,...) devolverá 
	una tupla con la secuencia original pero sin duplicados
		
	>>> remove_dup([])
	()
		
	>>> remove_dup((1, 2, 3))
	(1, 2, 3)
	
	>>> remove_dup((1, 2, 3, 2, 1))
	(1, 2, 3)
	
	>>> remove_dup([1, (1, 2), 3, (1, 2), 4, 3])
	(1, (1, 2), 3, 4)
	
	>>> remove_dup('hooooooolaaaaa')
	('h', 'o', 'l', 'a')
	
	Args:
		sec (tupla, lista, string...): Secuencia iterable
	Returns:
		(tupla): Secuencia original sin duplicados
	
	"""
	
	sec_sen_dup = []
	for e in sec:
		if e not in sec_sen_dup:
			sec_sen_dup.append(e)
	return tuple(sec_sen_dup)
	
#string => string
def elimina_tildes_tup(text):
	"""Devuelve el texto recibido tras substituir cada vocal con tilde,
	además de la 'ü' con diéresis, por su correspondiente sin signo diacrítico.
		
	>>> elimina_tildes_tup('prueba')
	'prueba'
		
	>>> elimina_tildes_tup('solicitará el número')
	'solicitara el numero'
	
	>>> elimina_tildes_tup('Función Única')
	'Funcion Unica'
	
	>>> elimina_tildes_tup('agüita con las diéresis')
	'aguita con las dieresis'
	
	Args:
		text (string): Texto
	Returns:
		(string): El texto recibido sin signos diacríticos
	
	"""
	cambios_tupla = (("á","a"),("é","e"),("í","i"),("ó","o"),("ú","u"),("ü","u"),
					 ("Á","A"),("É","E"),("Í","I"),("Ó","O"),("Ú","U"),("Ü","U"))
	new_text = ""
	for l in text:
		if l in [x[0] for x in cambios_tupla]:
			for t in cambios_tupla:
				if l==t[0]:
					new_text += t[1]
		else:
			new_text += l
	return new_text
	
#string => string
def elimina_tildes(text):
	"""Devuelve el texto recibido tras substituir cada vocal con tilde,
	además de la 'ü' con diéresis, por su correspondiente sin signo diacrítico.
		
	>>> elimina_tildes('prueba')
	'prueba'
		
	>>> elimina_tildes('solicitará el número')
	'solicitara el numero'
	
	>>> elimina_tildes('Función Única')
	'Funcion Unica'
	
	>>> elimina_tildes('agüita con las diéresis')
	'aguita con las dieresis'
	
	Args:
		text (string): Texto
	Returns:
		(string): El texto recibido sin signos diacríticos
	
	"""
	cambios_dict = {"á":"a","é":"e","í":"i","ó":"o","ú":"u","ü":"u",
					"Á":"A","É":"E","Í":"I","Ó":"O","Ú":"U","Ü":"U"}
	new_text = ""
	for l in text:
		if l in cambios_dict:
			new_text += cambios_dict[l]
		else:
			new_text += l
	return new_text
		

if __name__ == '__main__':
	import doctest
	doctest.testmod(verbose=True)
	